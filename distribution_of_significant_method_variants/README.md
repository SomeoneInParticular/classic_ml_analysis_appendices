Distributions of machine learning or MRI methodology variations found to have a significant impact on a performance metric's value when used.

All variations with a dataset contained here were found to have a statistically significant effect (`p <= 0.05`) on the corresponding performance metric, as assessed using Ordinary Least Squares ANOVA (not presented in publication at editor's request).

Potential variations are labelled as follows:
* `acq`: The orientation of the MRI sequence
* `weight`: The weighting of the the MRI sequence
* `method`: The segmentation method used to process the MRI sequence (`deepseg` or `propseg`)
* `is_pca`: Whether PCA was used in feature pre-processing
* `is_rfe`: Whether RFE was used in feature pre-processing

Each dataset contains the following columns:
* `Mean`: The average of the associated performance metric with the specified methodology variation
* `95% CI`: An estimate of the 95% confidence interval for all models trained with the specified methodology variation
* `Min.`: The minimum of the performance metric observed in our tests specified methodology variation
* `Max`: The maximum of the performance metric observed in our tests for the specified methodology variation
