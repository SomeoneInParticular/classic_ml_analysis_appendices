Feature importance values of best performing MRI/ML pipeline combination. As said model was a Logistic Regression, they correspond to the coefficients of said model after training.

`feature_importance_raw.tsv` contains the raw feature importance values from each replicate of the pipeline, as well as their mean, standard deviation, and magnitude.

`feature_importance_relative.tsv` containst the feature importance values for each replicate, standardized to control for differences in model coefficient scaling. The mean, standard deviation, and magnitude of these values is also provided.

The following acronyms are present in both datasets:
* STD: Standard Deviation
* RL: Right-Left
* AP: Anterior-Posterior
* MCS: The Mental Component Score of the Short-Form 12
* PCS: The Physical Component Score of the Short-Form 12
