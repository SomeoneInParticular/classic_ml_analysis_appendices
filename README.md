This repository hosts datasets to extensive to place within the main body of the publication (currently not available). STD = Standard Deviation, AP = Anterior-Posterior, RL = Right-Left.

Each of the folders contain the following:

1. `distribution_of_significant_method_variants`: Contains the distribution of performance metrics, grouped by analysis pipeline variations which were found to differ by a statistically significant degree (`p <= 0.05` as measured by OLS ANOVA).
    * Note that the "Poor" outcome groups is being labeled as "Fair" in this dataset, due to an older naming scheme used during the original analyses prior to publication.
2. `feature_distributions`: A summary of all predictor values used in the machine learning models, grouped into clinical, demographic, and image-derived groups. 
    * Categorical features have their categories listed, as well as the frequency of those classes. 
    * Continuous features have their mean, standard deviation, confidence interval for where we would expect 95% of samples to be, minimum, and maximum.
    * Distributions have been calculated for the full dataset, as well as the dataset of records which were successfully segmented with both DeepSeg and PropSeg for use in their respective models.
3. `feature_importance`: The relative feature importance for predictions made by the most successful pipeline tested.
    * Both the raw feature importance values, and their standardized values, are provided, though only the latter was discussed explicitly in the publication.
    * For categorical variables, the relevant category is provided following and underscore (i.e. Comorbidities: Nicotine (Recent Quit) True, which represents the importance of the “True” value for the “Comorbidities: Nicotine (Recent Quit)” feature).
4. `performance_by_protocol`: Contains all performance metrics measures for each protocol for each replicate. 
    * Note that the "Poor" outcome groups is being labeled as "Fair" in this dataset, due to an older naming scheme used during the original analyses prior to publication.


